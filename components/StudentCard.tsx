import React, { useCallback } from "react";
import { Card, CardContent, Typography, Box, Rating } from "@mui/material";
import Image from "next/image";
import { Student } from "@/interfaces/Student";
import { useRecoilState } from 'recoil';
import { sessionAtom } from '@/lib/atoms/sessionAtom'
import { studentAtom } from "@/lib/atoms/studentAtom";
import { useRouter } from 'next/router';

const sessionImage = [
  {
    absent: "/edv-char1-blk.png",
    present: "/edv-char1-color.png"
  },
  {
    absent: "/edv-char2-blk.png",
    present: "/edv-char2-color.png"
  },
  {
    absent: "/edv-char3-blk.png",
    present: "/edv-char3-color.png"
  },
  {
    absent: "/edv-char4-blk.png",
    present: "/edv-char4-color.png"
  },
  {
    absent: "/edv-char5-blk.png",
    present: "/edv-char5-color.png"
  },
  {
    absent: "/edv-char6-blk.png",
    present: "/edv-char6-color.png"
  }
];

const StudentCard = (props: any) => {
  const { student } = props;
  const { setStudentListsData } = props;
  const { setIsNewUpdate } = props;
  const router = useRouter();
  const [value, setValue] = React.useState<number | null>(0);
  const [sessionNumber, setSessionNumber] = useRecoilState(
    sessionAtom.sessionNumber
  );
  
  const session_key = "session_"+sessionNumber;
  const updateStudentStars = async (star_num?: any, stud_id?: string) => {
    const stud = {
      ...student,
      sessions: {
        ...student.sessions,
        [session_key]: {
          ...student?.sessions?.[session_key],
          ['stars']: star_num
        }
      }
    };
    fetch(process.env.BASE_URL+`/api/students/`+stud_id  as string, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(stud)
    }).then(async (msg)=>{
      const response_p = await fetch(process.env.BASE_URL+`/api/students` as string);
      const students = await response_p.json();
      setStudentListsData(students);
      setIsNewUpdate(true);
    });
  };

  const updateStudentPresent = async (present?: boolean, stud_id?: string) => {
    const stud = {
      ...student,
      sessions: {
        ...student.sessions,
        [session_key]: {
          ...student?.sessions?.[session_key],
          ['present']: present
        }
      }
    };
    fetch(process.env.BASE_URL+`/api/students/`+stud_id as string, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(stud)
    }).then(async (msg)=>{
      const response_p = await fetch(process.env.BASE_URL+`/api/students` as string);
      const students = await response_p.json();
      setStudentListsData(students);
      setIsNewUpdate(true);
    });
  };

  return (
    <Card sx={{ display: "flex", height: "130px", margin: "20px 0" }}>
      <Box flex={"1"} display={"flex"} flexDirection={"row"}>
        <Box
          flex={"3"}
          display={"flex"}
          flexDirection={"column"}
          justifyContent={"space-between"}
        >
          <CardContent>
            <Typography variant="body1">{student.student_name}</Typography>
          </CardContent>
          <CardContent>
            <Rating
              name="simple-controlled"
              value={student?.sessions?.[session_key]?.stars}
              onChange={(event, newValue) => {
                updateStudentStars(newValue, student?._id);
              }}
            />
          </CardContent>
        </Box>
        <Box flex={"0.5"} display={"flex"} justifyContent={"center"}>
          <Box alignSelf={"center"} width={88} height={111} onClick={()=> updateStudentPresent(true,student?._id)}>
            {student?.sessions?.[session_key]?.present ? (
              <Image
                src={sessionImage[sessionNumber-1]?.present}
                alt={""}
                width={88}
                height={111}
                style={{ objectFit: "cover" }}
              />
            ) : (
              <Image
                src={sessionImage[sessionNumber-1]?.absent}
                alt={""}
                width={88}
                height={111}
                style={{ objectFit: "cover" }}
              />
            )}
            
          </Box>
        </Box>
      </Box>
    </Card>
  );
};

export default StudentCard;
