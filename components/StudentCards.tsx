import React from "react";
import { Students } from "@/interfaces/Students";
import { Student } from "@/interfaces/Student";
import StudentCard from "./StudentCard";
import { Box,Skeleton } from '@mui/material'
import { useRecoilState, useRecoilTransactionObserver_UNSTABLE } from "recoil";
import { studentAtom } from '@/lib/atoms/studentAtom'


const StudentCards = (props: any) => {
  const { students } = props;
  const [studentListsData, setStudentListsData] = useRecoilState(
    studentAtom.studentsLists
  );
  const [isNewUpdate, setIsNewUpdate] = useRecoilState(
    studentAtom.isNewUpdate
  );
  React.useEffect(()=>{
    if (!isNewUpdate) {
      setStudentListsData(students);
    } else {
      setStudentListsData(studentListsData);
    }
  },[students, setStudentListsData, studentListsData, isNewUpdate]);

  
  return (
    <>
      {studentListsData ? studentListsData?.map((student, index) =>
          (
            <StudentCard key={index} student={student} setStudentListsData={setStudentListsData} setIsNewUpdate={setIsNewUpdate} />
          )
      ) : (
        <>
          <Skeleton />
          <Skeleton animation="wave" />
          <Skeleton animation={false} />
        </>
      )}
    </>
  );
}

export default StudentCards
