export interface Student {
    _id?: string,
    student_name?: string,
    sessions?: {
      [key: string]: any,
      session_1?: {
        stars?: Number,
        present?: Boolean
      },
      session_2?: {
        stars?: Number,
        present?: Boolean
      },
      session_3?: {
        stars?: Number,
        present?: Boolean
      },
      session_4?: {
        stars?: Number,
        present?: Boolean
      },
      session_5?: {
        stars?: Number,
        present?: Boolean
      },
      session_6?: {
        stars?: Number,
        present?: Boolean
      }
    }
}