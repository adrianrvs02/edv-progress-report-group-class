
import { atom } from "recoil";

export const sessionAtom = {
  sessionNumber: atom({
    key: "sessionNumber", // all the atoms in recoil have their unique key
    default: 1 // this is the default value of an atom
  }),
};