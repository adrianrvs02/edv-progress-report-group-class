
import { Students } from "@/interfaces/Students";
import { atom } from "recoil";

export const studentAtom = {
  studentsLists: atom({
    key: "studentsLists", // all the atoms in recoil have their unique key
    default: [] || null// this is the default value of an atom
  }),
  isNewUpdate: atom({
    key: "isNewUpdate", // all the atoms in recoil have their unique key
    default: false// this is the default value of an atom
  }),
};