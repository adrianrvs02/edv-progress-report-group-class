import mongoose, { Model } from "mongoose"

// CONNECTING TO MONGOOSE (Get Database Url from .env.local)
const { DATABASE_READERS_GROUP_CLASS_URL } = process.env

// connection function
export const mognoconnreadersgroupclass = async () => {
  const conn = await mongoose
    .connect(DATABASE_READERS_GROUP_CLASS_URL as string)
    .catch(err => console.log(err))
  console.log("Mongoose Connection Established")

  // OUR PROJECT SCHEMA
  const StudentSchema = new mongoose.Schema({
		student_name: String,
    sessions: {
      session_1: {
        stars: Number,
        present: Boolean
      },
      session_2: {
        stars: Number,
        present: Boolean
      },
      session_3: {
        stars: Number,
        present: Boolean
      },
      session_4: {
        stars: Number,
        present: Boolean
      },
      session_5: {
        stars: Number,
        present: Boolean
      },
      session_6: {
        stars: Number,
        present: Boolean
      }
    }
  })

  // OUR PROJECT MODEL
  const students = mongoose.models.students || mongoose.model("students", StudentSchema)

  return { conn, students }
}