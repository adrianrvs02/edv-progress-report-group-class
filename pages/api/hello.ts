// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'

type Data = {
  session_no: string
  session_question: string,
  students: object
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const sessionData = {
    session_no: '1',
    session_question: 'How was your awesomeness today?',
    students: {
      student_1: {
        name: 'Yuan Yuan 1',
        star_number: '0',
        picture: '',
      },
      student_2: {
        name: 'Yuan Yuan 1',
        star_number: '0',
        picture: '',
      },
      student_3: {
        name: 'Yuan Yuan 1',
        star_number: '0',
        picture: '',
      },
      student_4: {
        name: 'Yuan Yuan 1',
        star_number: '0',
        picture: '',
      },
      student_5: {
        name: 'Yuan Yuan 1',
        star_number: '0',
        picture: '',
      },
      student_6: {
        name: 'Yuan Yuan 1',
        star_number: '0',
        picture: '',
      },
      student_7: {
        name: 'Yuan Yuan 1',
        star_number: '0',
        picture: '',
      },
      student_8: {
        name: 'Yuan Yuan 1',
        star_number: '0',
        picture: '',
      },
      student_9: {
        name: 'Yuan Yuan 1',
        star_number: '0',
        picture: '',
      },
      student_10: {
        name: 'Yuan Yuan 1',
        star_number: '0',
        picture: '',
      },
      student_11: {
        name: 'Yuan Yuan 1',
        star_number: '0',
        picture: '',
      },
      student_12: {
        name: 'Yuan Yuan 1',
        star_number: '0',
        picture: '',
      },
    }
  }
  res.status(200).json(sessionData)
}
