// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { ResponseFunctions } from '../../../interfaces/ResponseFunctions'
import { mognoconnreadersgroupclass } from '../../../lib/mongodbcon/connection'


const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const method: keyof ResponseFunctions = req.method as keyof ResponseFunctions
  // catch errors
  const catcher = (error: Error) => res.status(400).json({ error })

  // Responses
  const handleCase: ResponseFunctions = {
    // Update REQUESTS
    PUT: async (req: NextApiRequest, res: NextApiResponse) => {
      const { students } = await mognoconnreadersgroupclass()
      res.json(await students.replaceOne({_id: req.query.id},req.body).catch(catcher))
    },
  }

  const response = handleCase[method]
  
  if (response) return response(req, res)
  else res.status(400).json({ error: "No Response for This Request" })
}

export default handler
